# Release Notes for Glue Auth

## Unreleased

## 5.0.1

- Craft 5 modal login support
- Remove forgotten console.log
- Fix redirect not working when a querystring is present

## 5.0.0

- Craft 5 support

## 4.0.11

- Add login history tracking

## 4.0.10

- Fix url generation for subsites

## 4.0.9

- Add support for subsite login
- Move redirect parameter to Craft Session

## 4.0.8

- Add redirect parameter

## 4.0.7

- Fix case compare for `isGlueUser` check
- Simplfy `cpTrigger` when to use `UrlHelper`

## 4.0.6

- Use `cpTrigger` when redirecting to dashboard

## 4.0.5

- Add Data encryption in transit

## 4.0.4

- LoginFrom js refactoring to only trigger on cp login route
- LoginModal js refactoring, styling and extraction from LoginForm
- Auto elevate User Sessions for Glue Admin users

## 4.0.3

- Add Glue button to session end LoginModal

## 4.0.2

- Use `@baseUrl` to generate the site url

## 4.0.1

- Remove Template Mode CP for admin template overwrite
- Add timeout to oath up check
- Add license to composer.json

## 4.0.0

- Initial Craft 4 release

## 3.0.1

- Remove Template Mode CP for admin template overwrite
- Add timeout to oath up check
- Add license to composer.json

## 3.0.0

- Initial Craft 3 release
