<?php

namespace GlueAgency\GlueAuth\utilities;

use Craft;
use craft\base\Utility;
use GlueAgency\GlueAuth\GlueAuth;

class GlueAuthUtility extends Utility
{

    public static function displayName(): string
    {
        return Craft::t('glue-auth', 'Glue Auth');
    }

    public static function id(): string
    {
        return 'glue-auth';
    }

    public static function icon(): ?string
    {
        return Craft::getAlias('@glue-auth') . '/icon-mask.svg';
    }

    public static function contentHtml(): string
    {
        return Craft::$app->getView()->renderTemplate('glue-auth/utilities/index.twig', [
            'paginator' => GlueAuth::getInstance()->loginHistory->paginateLoginHistory(
                Craft::$app->request->getPageNum(),
                200
            ),
        ]);
    }
}
