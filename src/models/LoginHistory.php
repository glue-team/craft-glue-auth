<?php

namespace GlueAgency\GlueAuth\models;

use Craft;
use craft\base\Model;
use craft\elements\User;
use DateTime;

class LoginHistory extends Model
{

    public ?int $id = null;

    public ?int $userId = null;

    public ?User $user = null;

    public ?DateTime $dateCreated;

    public ?string $uid = null;

    public function getUser(): ?User
    {
        if(! $this->userId) {
            return null;
        }

        if($this->user instanceof User) {
            return $this->user;
        }

        $this->user = Craft::$app->getUsers()->getUserById($this->userId);

        return $this->user;
    }
}
