<?php

namespace GlueAgency\GlueAuth\models;

use craft\base\Model;

class Settings extends Model
{

    public bool $enabled = true;

    public string $proxyUrl = 'https://oauth.glue.be/oauth/callback';

    public string $encryptionToken;
}
