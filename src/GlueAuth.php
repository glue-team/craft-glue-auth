<?php

namespace GlueAgency\GlueAuth;

use Craft;
use craft\base\Plugin;
use craft\controllers\UsersController;
use craft\elements\User;
use craft\events\AuthenticateUserEvent;
use craft\events\LoginFailureEvent;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\RegisterUrlRulesEvent;
use craft\log\MonologTarget;
use craft\services\Plugins;
use craft\services\Utilities;
use craft\web\UrlManager;
use craft\web\View;
use Exception;
use GlueAgency\GlueAuth\models\Settings;
use GlueAgency\GlueAuth\services\LoginHistoryService;
use GlueAgency\GlueAuth\services\LoginService;
use GlueAgency\GlueAuth\services\TokenService;
use GlueAgency\GlueAuth\services\UserService;
use GlueAgency\GlueAuth\utilities\GlueAuthUtility;
use Monolog\Formatter\LineFormatter;
use Psr\Log\LogLevel;
use yii\base\Event;
use yii\log\Logger;

/**
 * Glue Auth Plugin
 *
 * @property LoginHistoryService $loginHistory
 * @property LoginService $login
 * @property TokenService $token
 * @property UserService  $user
 *
 * @property Settings     $settings
 *
 * @method static GlueAuth getInstance()
 */
class GlueAuth extends Plugin
{
    public static GlueAuth $instance;

    public function init(): void
    {
        parent::init();

        Craft::setAlias('@glue-auth', $this->getBasePath());

        self::$instance = $this;

        // Register Components (Services)
        $this->setComponents([
            'loginHistory' => LoginHistoryService::class,
            'login' => LoginService::class,
            'token' => TokenService::class,
            'user' => UserService::class
        ]);

        // Defer most setup tasks until Craft is fully initialized
        Craft::$app->onInit(function () {
            $this->_registerLogTarget();
            $this->attachEventHandlers();

            $this->registerUtilities();

            $this->login->elevateUserSession();
        });

        Event::on(Plugins::class, Plugins::EVENT_AFTER_LOAD_PLUGINS, function() {
            if(GlueAuth::getInstance()->settings->enabled) {
                $this->login->registerCpLogin();
            }
        });
    }

    protected function createSettingsModel(): Settings
    {
        return new Settings;
    }

    private function attachEventHandlers(): void
    {
        Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function(RegisterTemplateRootsEvent $event) {
            $event->roots['glue-auth'] = __DIR__ . '/templates';
        });

        Event::on(UrlManager::class, UrlManager::EVENT_REGISTER_SITE_URL_RULES, function (RegisterUrlRulesEvent $event) {
            $event->rules['glue-auth/auth/callback'] = 'glue-auth/auth/callback';
        });

        Event::on(User::class, User::EVENT_BEFORE_AUTHENTICATE, function(AuthenticateUserEvent $event) {
            $user = $event->sender;

            if(! GlueAuth::getInstance()->settings->enabled) {
                return;
            }

            if(GlueAuth::getInstance()->login->isGlueUser($user) && $user->admin) {
                // Try and connect to the oAuth server
                try {
                    $client = Craft::createGuzzleClient([
                        'timeout' => 3,
                        'connect_timeout'=> 3,
                    ]);
                    $response = $client->head(GlueAuth::getInstance()->settings->proxyUrl);

                    if($response->getStatusCode() !== 200) {
                        return;
                    }
                } catch (Exception $e) {
                    // Cant connect to the oauth server, allow logins
                    return;
                }

                // Set our custom message
                $user->authError = 'glue_auth_user_block';
            }
        });

        Event::on(UsersController::class, UsersController::EVENT_LOGIN_FAILURE, function(LoginFailureEvent $event) {
            if($event->authError === 'glue_auth_user_block') {
                // Translate the custom message
                $event->message = Craft::t('glue-auth', 'Regular login disabled for Glue admin users.');
            }
        });
    }

    public function registerUtilities(): void
    {
        Event::on(Utilities::class, Utilities::EVENT_REGISTER_UTILITIES, function(RegisterComponentTypesEvent $event) {
            $event->types[] = GlueAuthUtility::class;
        });
    }

    /**
     * Logs an informational message to our custom log target.
     */
    public static function info(string $message): void
    {
        Craft::info($message, 'glue-auth');
    }

    /**
     * Logs an error message to our custom log target.
     */
    public static function error($message, $attributes = []): void
    {
        if ($attributes) {
            $message = Craft::t('glue-auth', $message, $attributes);
        }

        Craft::getLogger()->log($message, Logger::LEVEL_ERROR, 'glue-auth');
    }

    /**
     * Registers a custom log target, keeping the format as simple as possible.
     */
    private function _registerLogTarget(): void
    {
        Craft::getLogger()->dispatcher->targets[] = new MonologTarget([
            'name' => 'glue-auth',
            'categories' => ['glue-auth'],
            'level' => LogLevel::INFO,
            'logContext' => false,
            'allowLineBreaks' => false,
            'formatter' => new LineFormatter(
                format: "%datetime% %message%\n",
                dateFormat: 'Y-m-d H:i:s',
            ),
        ]);
    }
}
