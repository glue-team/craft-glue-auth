<?php

namespace GlueAgency\GlueAuth\migrations;

use craft\db\Migration;

/**
 * Install migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp(): bool
    {
        $this->createTable('{{%glue_auth_tokens}}', [
            'id' => $this->primaryKey(),
            'accessToken' => $this->text(),
            'expires' => $this->string(),
            'resourceOwnerId' => $this->string(),
            'dateCreated' => $this->dateTime()->notNull(),
            'dateUpdated' => $this->dateTime()->notNull(),
            'uid' => $this->uid(),
        ]);

        $this->createTable('{{%glue_auth_login_history}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'dateCreated' => $this->dateTime()->notNull(),
            'uid' => $this->uid(),
        ]);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown(): bool
    {
        if ($this->db->tableExists('{{%glue_auth_tokens}}')) {
            $this->dropTable('{{%glue_auth_tokens}}');
        }

        if ($this->db->tableExists('{{%glue_auth_login_history}}')) {
            $this->dropTable('{{%glue_auth_login_history}}');
        }

        return true;
    }
}
