<?php

namespace GlueAgency\GlueAuth\migrations;

use craft\db\Migration;

/**
 * m231129_085442_create_login_history_table migration.
 */
class m231129_085442_create_login_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp(): bool
    {
        $this->createTable('{{%glue_auth_login_history}}', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'dateCreated' => $this->dateTime()->notNull(),
            'uid' => $this->uid(),
        ]);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function safeDown(): bool
    {
        echo "m231129_085442_create_login_history_table cannot be reverted.\n";

        return false;
    }
}
