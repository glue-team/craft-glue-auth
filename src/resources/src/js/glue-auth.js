if (typeof Craft.GlueAuth === typeof undefined) {
  Craft.GlueAuth = {};
}

(function ($) {
  Craft.GlueAuth.CustomLoginForm = Garnish.Base.extend({
    init (settings) {
      this.html = settings.html;

      this.form = $('form.login-form');
      this.username = $('form.login-form input[name="username"]');
      this.password = $('form.login-form input[name="password"]');
      this.submit = $('form.login-form button[type="submit"]');

      this.bindLoginButton();

      if (this.form.length) {
        this.renderLoginForm();
      }
    },

    renderLoginForm() {
      // Get the existing login button
      this.submit.removeClass('btngroup-btn-first');
      const originalSubmit = this.submit.detach();

      // Add our button and group
      $(this.html).appendTo(this.form);

      // Add the existing button back to the new group
      $(originalSubmit).appendTo(this.form.find('.btngroup'));
    },

    bindLoginButton() {
      $(document).on('mouseup', 'button[data-glue-auth]', (e) => {
        e.preventDefault();

        const form = $('form#x');

        Craft.submitForm(form, {
          action: 'glue-auth/auth/login',
          redirect: null,
          params: {
            loginName: Craft.username,
          }
        });
      });
    },
  });

  Craft.GlueAuth.CustomLoginModal = Garnish.Base.extend({
    init (settings) {
      const self = this;
      this.html = settings.html;
      this.renderedLogin = false;

      this.bindLoginButton();

      // Setup session-ended login form. More involved becuase it's triggered via JS
      // So we need to watch for the dynamically-added element
      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          mutation.addedNodes.forEach((addedNode) => {
            if (addedNode.classList?.contains('login-modal')) {
              self.renderLoginModalForm(addedNode);
            }
          });
        });
      });

      observer.observe(document.body, {childList: true, subtree: true});
    },

    renderLoginModalForm(form) {
      const $loginModal = $(form);

      const $formButton = $loginModal.find('button[type=submit]');
      const $wrapper = $formButton.parent();

      // Only insert it once, as due to session-pinging, this can fire multiple times
      if (this.renderedLogin) {
        return;
      }

      let $html = $(this.html);

      // Move the original login button to the
      // custom button group and remove the existing btngroup-btn-first class
      $formButton
        .removeClass('btngroup-btn-first')
        .appendTo($html);

      // Add our custom button to the form
      $html.appendTo($wrapper);

      // Resize the modal to fit
      $loginModal.trigger('updateSizeAndPosition');
      $(window).trigger('resize');

      this.renderedLogin = true;
    },

    bindLoginButton() {
      $(document).on('mouseup', 'button[data-glue-auth]', (e) => {
        e.preventDefault();

        Craft.redirectTo(Craft.getActionUrl('glue-auth/auth/login'));
      });
    }
  })
})(jQuery);
