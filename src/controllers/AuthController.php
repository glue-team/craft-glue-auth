<?php

namespace GlueAgency\GlueAuth\controllers;

use Craft;
use craft\helpers\UrlHelper;
use craft\web\Controller;
use DateTime;
use GlueAgency\GlueAuth\GlueAuth;
use GlueAgency\GlueAuth\Helpers\Encryptor;
use GlueAgency\GlueAuth\models\LoginHistory;
use GlueAgency\GlueAuth\profiles\OAuthProfile;
use GlueAgency\GlueAuth\profiles\UserProfile;
use RuntimeException;
use Spatie\Url\Url;
use yii\web\Response;

class AuthController extends Controller
{

    public $defaultAction = 'index';

    protected array|int|bool $allowAnonymous = [
        'login',
        'callback',
    ];

    public function beforeAction($action): bool
    {
        if($action->id === 'callback') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionLogin(): Response
    {
        GlueAuth::getInstance()->token->purgeExpiredTokens();

        $proxy = GlueAuth::getInstance()->settings->proxyUrl;

        // Use request referer to get the current site
        // because Craft::$app->getSites()->getCurrentSite()
        // always returns the primary site.
        $request = Craft::$app->getRequest();
        $site = $request->getReferrer() ?: Craft::$app->getSites()->getPrimarySite()->getBaseUrl();
        $url = Url::fromString($site)
            ->withPath('/glue-auth/auth/callback')
            ->withoutQueryParameters()
            ->withFragment('');
        $site = (string) $url;

        $token = GlueAuth::getInstance()->token->createToken();

        // Allow passing in a `redirect` param to redirect to upon callback
        $redirect = Craft::$app->getSecurity()->validateData($request->getParam('redirect'));
        $redirect = $redirect ?: $request->getReferrer();
        $redirect = Url::fromString($redirect);

        // Verify the redirect url is not the same as the cp login url
        // Craft 5 does not auto redirect logged-in users when they land
        // on the login page again
        $cpTrigger = Craft::$app->getConfig()->getGeneral()->cpTrigger;
        if($redirect->getPath() === "/{$cpTrigger}/login") {
            $redirect = $redirect->withPath("/{$cpTrigger}/dashboard");
        }

        // Set the redirect
        Craft::$app->getSession()->set('glue-auth-redirect', (string) $redirect);

        return $this->redirect(UrlHelper::url($proxy, [
            'site'  => $site,
            'token' => $token->accessToken,
        ]));
    }

    public function actionCallback(): Response
    {
        $oAuthProfile = new OAuthProfile(Craft::$app->getRequest()->getQueryParams());

        if(! $token = GlueAuth::getInstance()->token->getByAccessToken($oAuthProfile->getAttribute('token'))) {
            Craft::$app->getSession()->setError(Craft::t('glue-auth', 'Access token expired.'));

            return $this->redirect(UrlHelper::cpUrl('login'));
        }

        GlueAuth::getInstance()->token->deleteToken($token);

        try {
            $encryptor = new Encryptor;

            $data = $encryptor->decrypt($oAuthProfile->data);
        } catch(RuntimeException $e) {
            Craft::$app->getSession()->setError(Craft::t('glue-auth', 'Invalid callback data.'));

            return $this->redirect(UrlHelper::cpUrl('login'));
        }

        $userProfile = new UserProfile($data);

        if($user = GlueAuth::getInstance()->user->findOrCreate($userProfile)) {
            if(Craft::$app->getUser()->login($user)) {
                GlueAuth::getInstance()->loginHistory->saveLoginHistory(new LoginHistory([
                    'userId'      => $user->id,
                    'dateCreated' => new DateTime,
                ]));
            } else {
                Craft::$app->getSession()->setError(Craft::t('glue-auth', 'Unable to login.'));
            }
        }

        $redirect = Craft::$app->getSession()->get('glue-auth-redirect') ?? UrlHelper::cpUrl('dashboard');

        return $this->redirect($redirect);
    }
}
