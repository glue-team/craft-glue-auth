<?php
namespace GlueAgency\GlueAuth\assetbundles;

use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

class GlueAuthAsset extends AssetBundle
{

    public function init(): void
    {
        $this->sourcePath = '@glue-auth/resources/src';

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/glue-auth.js',
        ];

        parent::init();
    }
}
