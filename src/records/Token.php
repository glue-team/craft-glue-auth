<?php

namespace GlueAgency\GlueAuth\records;

use Craft;
use craft\db\ActiveRecord;

/**
 * Token record
 */
class Token extends ActiveRecord implements \Stringable
{
    // Static Methods
    // =========================================================================

    public static function tableName(): string
    {
        return '{{%glue_auth_tokens}}';
    }

    public function __toString()
    {
        return $this->accessToken;
    }
}
