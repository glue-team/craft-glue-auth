<?php

namespace GlueAgency\GlueAuth\records;

use craft\db\ActiveRecord;
use craft\records\User;

class LoginHistory extends ActiveRecord
{

    public static function tableName(): string
    {
        return '{{%glue_auth_login_history}}';
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'userId']);
    }
}
