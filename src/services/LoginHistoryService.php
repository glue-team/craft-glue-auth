<?php

namespace GlueAgency\GlueAuth\services;

use craft\base\Component;
use craft\db\Paginator;
use craft\db\Query;
use craft\helpers\Db;
use craft\helpers\StringHelper;
use DateTime;
use GlueAgency\GlueAuth\models\LoginHistory;
use GlueAgency\GlueAuth\records\LoginHistory as LoginHistoryRecord;

class LoginHistoryService extends Component
{

    public function getAllLoginHistory(): array
    {
        $models = [];
        $records = $this->_createLoginHistoryQuery()
            ->orderBy(['dateCreated' => SORT_DESC])
            ->all();

        foreach($records as $record) {
            $models[] = new LoginHistory($record);
        }

        return $models;
    }

    public function paginateLoginHistory(int $currentPage = 1, int $pageSize = 20): Paginator
    {
        $query = $this->_createLoginHistoryQuery()
            ->orderBy(['dateCreated' => SORT_DESC]);

        $paginator = new Paginator($query, [
            'currentPage' => $currentPage,
            'pageSize' => $pageSize,
        ]);

        $models = [];
        foreach($paginator->getPageResults() as $result) {
            $models[] = new LoginHistory($result);
        }

        $paginator->setPageResults($models);

        return $paginator;
    }

    public function saveLoginHistory(LoginHistory $loginHistory): bool
    {
        $this->saveLoginHistoryInternal($loginHistory);

        return true;
    }

    private function saveLoginHistoryInternal(LoginHistory $loginHistory): void
    {
        $isNew = ! $loginHistory->id;

        if($isNew) {
            $loginHistoryRecord = new LoginHistoryRecord;

            if(! $loginHistoryRecord->dateCreated) {
                $loginHistoryRecord->dateCreated = new DateTime;
            }

            if(! $loginHistoryRecord->uid) {
                $loginHistoryRecord->uid = StringHelper::UUID();
            }
        } else {
            $loginHistoryRecord = LoginHistoryRecord::findOne($loginHistory->id);
        }

        $loginHistoryRecord->userId = $loginHistory->userId;
        $loginHistoryRecord->dateCreated = Db::prepareDateForDb($loginHistory->dateCreated);

        $loginHistoryRecord->save();

        $loginHistory->id = $loginHistoryRecord->id;
        $loginHistory->uid = $loginHistoryRecord->uid;
    }

    private function _createLoginHistoryQuery(): Query
    {
        return (new Query())
            ->select([
                'id',
                'userId',
                'dateCreated',
                'uid',
            ])
            ->from([LoginHistoryRecord::tableName()]);
    }
}
