<?php

namespace GlueAgency\GlueAuth\services;

use Craft;
use craft\base\Component;
use craft\elements\User;
use craft\helpers\Json;
use craft\helpers\Session as SessionHelper;
use GlueAgency\GlueAuth\assetbundles\GlueAuthAsset;
use GlueAgency\GlueAuth\GlueAuth;
use Throwable;

class LoginService extends Component
{

    public function registerCpLogin(): void
    {
        $request = Craft::$app->getRequest();
        $view = Craft::$app->getView();

        try {
            if ($request->getIsCpRequest()) {
                $view->registerAssetBundle(GlueAuthAsset::class);

                if($request->getSegment(1) == 'login') {
                    $view->registerJs('new Craft.GlueAuth.CustomLoginForm(' . Json::encode([
                        'html' => $view->renderTemplate('glue-auth/cp-login'),
                    ]) . ');');
                } else {
                    $view->registerJs('new Craft.GlueAuth.CustomLoginModal(' . Json::encode([
                        'html' => $view->renderTemplate('glue-auth/cp-modal-login')
                    ]) . ');');
                }
            }
        } catch (Throwable $e) {
            GlueAuth::error('Unable to render CP Login template: “{message}” {file}:{line}', [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
            ]);
        }
    }

    public function elevateUserSession():void
    {
        $user = Craft::$app->getUser();

        if($user &&
            $user->identity &&
            $user->identity->admin &&
            $this->isGlueUser($user->identity)
        ) {
            $timeout = time() + Craft::$app->getConfig()->getGeneral()->elevatedSessionDuration;
            SessionHelper::set($user->elevatedSessionTimeoutParam, $timeout);
        }
    }

    public function isGlueUser(User $user): bool
    {
        preg_match('/.+@(\S+)/i', $user->email, $matches);

        [, $domain] = $matches;

        return strcasecmp($domain, 'glue.be') === 0;
    }
}
