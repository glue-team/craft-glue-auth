<?php

namespace GlueAgency\GlueAuth\services;

use Craft;
use craft\base\Component;
use craft\elements\User;
use GlueAgency\GlueAuth\GlueAuth;
use GlueAgency\GlueAuth\profiles\UserProfile;
use Throwable;

class UserService extends Component
{

    public function findOrCreate(UserProfile $profile): User
    {
        $user = User::find()->email($profile->getEmail())->one();

        if(! $user) {
            $user = new User;

            $user->username = $profile->getEmail();
            $user->email = $profile->getEmail();
            $user->fullName = $profile->getAttribute('name');
            $user->firstName = $profile->getAttribute('given_name');
            $user->lastName = $profile->getAttribute('family_name');

            // Make this user an admin
            $user->admin = true;

            if(! Craft::$app->getElements()->saveElement($user)) {
                Craft::$app->getSession()->setError(Craft::t('glue-auth', 'Could not save user'));

                GlueAuth::error('Could not save user: {errors}', [
                    'errors' => $user->getErrorSummary(true)
                ]);
            }
        }

        try {
            Craft::$app->getUsers()->activateUser($user);
        } catch (Throwable $e) {
            Craft::$app->getSession()->setError(Craft::t('glue-auth', 'Could not activate user'));

            GlueAuth::error('Could not activate user: “{message}”', [
                'message' => $e->getMessage(),
            ]);
        }

        return $user;
    }
}
