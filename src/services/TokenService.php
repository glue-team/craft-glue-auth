<?php

namespace GlueAgency\GlueAuth\services;

use Carbon\Carbon;
use Craft;
use craft\db\Query;
use craft\helpers\Db;
use GlueAgency\GlueAuth\records\Token as TokenRecord;
use yii\base\Component;

class TokenService extends Component
{

    public function getAllTokens(): array
    {
        return $this->_createTokenQuery()->all();
    }

    public function getTokenById(int $id): ?TokenRecord
    {
        $result = TokenRecord::findOne($id);

        return $result ?? null;
    }

    public function createToken(): TokenRecord
    {
        $token = new TokenRecord();

        $accessToken = Craft::$app->getSecurity()->generateRandomString(24);
        $now = new Carbon;
        $expires = $now->add('2 hours');
        $token->expires = Db::prepareDateForDb($expires);
        $token->accessToken = $accessToken;

        $token->save();

        return $token;
    }

    public function getByAccessToken(string $accessToken): ?TokenRecord
    {
        return TokenRecord::find()
            ->where(['=', 'accessToken', $accessToken])
            ->andWhere(['>=', 'expires', Db::prepareDateForDb(Carbon::now())])
            ->one();
    }

    public function purgeExpiredTokens(): void
    {
        TokenRecord::deleteAll(['<=', 'expires', Db::prepareDateForDb(Carbon::now())]);
    }

    public function deleteToken(TokenRecord $token): bool
    {
        return $token->delete();
    }

    private function _createTokenQuery(): Query
    {
        return (new Query())
            ->select([
                'id',
                'accessToken',
                'expires',
                'resourceOwnerId',
                'dateCreated',
                'dateUpdated',
                'uid',
            ])
            ->from([TokenRecord::tableName()]);
    }

    private function _getTokenRecordById(?int $tokenId = null): TokenRecord
    {
        if(($tokenId !== null) && $tokenRecord = TokenRecord::findOne($tokenId)) {
            return $tokenRecord;
        }

        return new TokenRecord();
    }
}
