<?php

namespace GlueAgency\GlueAuth\profiles;

class AbstractProfile
{

    protected array $original = [];

    protected array $attributes = [];

    public function __construct(array $data)
    {
        $this->attributes = $this->original = $data;
    }

    public function getOriginal($name): mixed
    {
        if(in_array($name, array_keys($this->original))) {
            return $this->original[$name];
        }

        return null;
    }

    public function getAttribute($name): mixed
    {
        if(in_array($name, array_keys($this->attributes))) {
            return $this->attributes[$name];
        }

        return null;
    }

    public function setAttribute($name, $value): self
    {
        $this->attributes[$name] = $value;

        return $this;
    }

    public function isDirty($name): bool
    {
        return $this->getAttribute($name) !== $this->getOriginal($name);
    }

    public function __get(string $name): mixed
    {
        return $this->getAttribute($name);
    }
}
