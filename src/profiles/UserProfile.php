<?php

namespace GlueAgency\GlueAuth\profiles;

class UserProfile extends AbstractProfile
{

    public function getUsername(): string
    {
        return $this->attributes['name'];
    }

    public function getEmail(): string
    {
        return $this->attributes['email'];
    }
}
