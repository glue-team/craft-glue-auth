<?php

namespace GlueAgency\GlueAuth\Helpers;

use GlueAgency\GlueAuth\GlueAuth;
use Illuminate\Encryption\Encrypter;

class Encryptor extends Encrypter
{

    public function __construct()
    {
        parent::__construct(
            base64_decode(GlueAuth::getInstance()->getSettings()->encryptionToken),
            'AES-256-CBC'
        );
    }
}
