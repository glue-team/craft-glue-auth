# Image Captions

This plugin adds a "G" button to the Craft Admin login. This simplifies logging in to any CMS set up by Glue-Agency.
Works for Local and Remote environments using a Google OAuth proxy.

## Requirements

This plugin requires Craft CMS 5.0.0 or later, and PHP 8.2 or later.

## Installation

Create a `glue-auth.php` file in the `/config` folder with this config

```php
return [
    // This token is used to decrypt data sent
    // from the oAuth proxy
    'encryptionToken' => getenv('GLUE_AUTH_ENCRYPTION_TOKEN'),
];
```

Finally add the env vars to your .env and .env.example.

### With Composer

Open your terminal and run the following commands

```bash
# Require the plugin through composer
composer require glue-agency/craft-glue-auth

# Install the plugin
php craft plugin/install glue-auth
```
